---
sidebar_position: 1
---

# One World Cafe

Serving the community by offering a gathering space where everyone could enjoy Midwest hospitality while enjoying globally inspired flavors.

## Website
[One World Cafe](https://www.oneworld-cafe.com/)

## Hours
Sunday - Thursday: 8:00am - 7:00pm

Friday - Saturday: 8:00am - 8:00pm

## Address
1245 W Main St
Peoria, IL 61606

## Phone
309.672.1522