---
sidebar_position: 2
---

# Radish Kitchen

Radish Kitchen is a plant-based and vegan hub for the Heart of Illinois

Radish Kitchen will continue the operation of pre-ordered meals in addition to grab-and-go meals, made-to-order meals, bulk vegan items, vegan and plant-based products from local vendors, bulk teas, fresh juice, soups, and more

## Website
[Radish Kitchen](https://radishkitchen.org/)

## Hours
Tusday - Saturday: 10:00am - 6:00pm

Sunday Brunch 9:00am - 3:00pm

## Address
1200 W Main St #22 Peoria, IL 61606

## Phone
309-839-2776